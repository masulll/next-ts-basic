import { useRouter } from "next/router";

const shopPage = () => {
  const router = useRouter();
  const { query } = router;

  // Check if query is defined before accessing its properties
  const category = query.category && query.category[0];
  const type = query.category && query.category[1];
  return (
    <div>
      <h1>
        Masukkan Category : {category} - {type}{" "}
      </h1>
    </div>
  );
};
export default shopPage;
