import { Config } from "@/Config";
import { useQuery } from "@tanstack/react-query";
import axios from "axios";
import { Spinner } from "react-bootstrap";

const apiClient = axios.create({
  baseURL: `${Config.newsApiURL}`,
});

export const useFetchNews = () => {
  return useQuery({
    queryKey: ["news"],
    queryFn: async () => {
      try {
        const newsResponse = await apiClient.get("/top-headlines", {
          params: {
            country: "us",
            apiKey: `${Config.newsApiKey}`,
          },
        });

        return newsResponse.data;
      } catch (error) {
        console.error("Failed to fetch news data:", error);
        throw new Error("Failed to fetch news data.");
      }
    },
  });
};

interface Props {
  title: string;
}

const NewsComponent: React.FC<Props> = ({ title }) => {
  const { data, isLoading, isError, error } = useFetchNews();

  if (isLoading)
    return (
      <div>
        <Spinner />
      </div>
    );
  if (isError) return <div className="text-error">Error: {error.message}</div>;

  return (
    <div>
      <h1>What's Up - {title}</h1>
      <ul>
        {data.articles.map((article: any, index: number) => (
          <li key={index}>{article.title}</li>
        ))}
      </ul>
    </div>
  );
};

export default NewsComponent;
