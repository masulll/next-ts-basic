import { Nav, Navbar, Container, Form, InputGroup } from "react-bootstrap";
import Image from "next/image";
import Link from "next/link";
import { css } from "@emotion/css";
import styled from "@emotion/styled";

const NavbarComponent: React.FC = () => {
  const binarColor = "linear-gradient(225deg, #6c1b72, #b62ab6)";

  const InputForm = styled.input`
    color: hotpink;
  `;

  const customInputGroup = css`
    input {
      background-color: white;
      border: none;
      &focus: {
        background-color: white;
        border: none;
      }
    }
  `;

  const linkStyling = css`
    color: white;
  `;

  return (
    <>
      <Navbar
        className={css`
          background-image: ${binarColor};
          padding: 1rem;
          color: white;
        `}
        fixed="top"
        expand="lg"
      >
        <Container>
          <Navbar.Brand
            href="/"
            className={`${linkStyling} d-flex align-items-center gap-2`}
          >
            <Image alt="" src="/assets/Logo_Binar.png" width="24" height="24" />
            News.Org
          </Navbar.Brand>
          <Nav className="ml-auto">
            <Link href={"/"} passHref legacyBehavior>
              <Nav.Link className={`${linkStyling}`}>Home</Nav.Link>
            </Link>
            <Link href={"/about"} passHref legacyBehavior>
              <Nav.Link className={`${linkStyling}`}>About</Nav.Link>
            </Link>
            <Form>
              <InputGroup className={`${customInputGroup}`}>
                <Form.Control
                  placeholder="Search"
                  aria-label="Search"
                  aria-describedby="basic-addon1"
                />
              </InputGroup>
              {/* <Button>test</Button> */}
              <label>Emotion CSS</label>
              <InputForm></InputForm>
            </Form>
          </Nav>
        </Container>
      </Navbar>
    </>
  );
};

export default NavbarComponent;
