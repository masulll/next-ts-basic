"use client";
import { css } from "@emotion/css";
import Link from "next/link";

const Footer: React.FC = () => {
  const binarColor = "linear-gradient(225deg, #6c1b72, #b62ab6)";

  const footerLinkStyle = css`
    font-weight: light;
    text-decoration: none;
    color: rgba(255, 255, 255, 0.8);
    transition: all 0.3s ease;
    &:hover {
      cursor: pointer;
      color: white;
    }
  `;

  const footerLayout = css`
    display: flex;
    flex-direction: column;
  `;
  return (
    <div
      className={css`
        display: flex;
        align-items: center;
        justify-content: center;
        padding: 32px;
        padding-bottom: 20px;
        gap: 15rem;
        color: white;
        background-image: ${binarColor};
        font-size: 16px;
      `}
    >
      <div className={footerLayout}>
        <h3>Navigation</h3>
        <Link href={"/"} className={footerLinkStyle}>
          Home
        </Link>
        <Link href={"/"} className={footerLinkStyle}>
          News
        </Link>
        <Link href={"/"} className={footerLinkStyle}>
          Login
        </Link>
      </div>
      <div className={footerLayout}>
        <h3>Help</h3>
        <Link href={"/"} className={footerLinkStyle}>
          FAQ
        </Link>
        <Link href={"/"} className={footerLinkStyle}>
          Privacy Policy
        </Link>
        <Link href={"/"} className={footerLinkStyle}>
          Terms & Conditions
        </Link>
      </div>
      <div className={footerLayout}>
        <h3>Address</h3>
        <p className={footerLinkStyle}>
          Lorem ipsum dolor sit, amet consectetur adipisicing elit. Nihil, amet
          provident.
        </p>
      </div>
    </div>
  );
};

export default Footer;
