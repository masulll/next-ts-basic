"use client";
import {
  useQuery,
  useMutation,
  useQueryClient,
  QueryClient,
  QueryClientProvider,
} from "@tanstack/react-query";

import { Container } from "react-bootstrap";
import Footer from "../Footer";
import Navbar from "../Navbar";
import { css } from "@emotion/css";

const AppShell = ({ children }: { children: React.ReactNode }) => {
  const queryClient = new QueryClient();

  return (
    <QueryClientProvider client={queryClient}>
      <div>
        <Navbar />
        <Container>{children}</Container>
        <Footer />
      </div>
    </QueryClientProvider>
  );
};

export default AppShell;
