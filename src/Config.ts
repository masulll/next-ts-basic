export const Config = {
  newsApiURL: process.env.NEXT_PUBLIC_NEWS_API_BASE_URL,
  newsApiKey: process.env.NEXT_PUBLIC_NEWS_API_KEY,
};
