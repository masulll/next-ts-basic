namespace NodeJS {
  interface ProcessEnv {
    NEXT_PUBLIC_NEWS_API_BASE_URL: string;
    NEXT_PUBLIC_NEWS_API_KEY: string;
  }
}
